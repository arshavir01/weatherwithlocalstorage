package com.ash.weatherwithlocalstorage.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WeatherCurrent(
    @Expose
    @SerializedName("dt")
    val dt: Long? = null,

    @Expose
    @SerializedName("sunrise")
    val sunrise: Long? = null,

    @Expose
    @SerializedName("sunset")
    val sunset: Long? = null,

    @Expose
    @SerializedName("temp")
    val temp: Double? = null,

    @Expose
    @SerializedName("feels_like")
    val feels_like: Double? = null,

    @Expose
    @SerializedName("pressure")
    val pressure: Int? = null,

    @Expose
    @SerializedName("humidity")
    val humidity: Int? = null,

    @Expose
    @SerializedName("dew_point")
    val dew_point: Double? = null,

    @Expose
    @SerializedName("uvi")
    val uvi: Double? = null,

    @Expose
    @SerializedName("clouds")
    val clouds: Double? = null,

    @Expose
    @SerializedName("visibility")
    val visibility: Double? = null,

    @Expose
    @SerializedName("wind_speed")
    val wind_speed: Double? = null,

    @Expose
    @SerializedName("wind_deg")
    val wind_deg: Double? = null,

    @Expose
    @SerializedName("weather")
    val weather: List<WeatherHourlySub>? = null
) {
}