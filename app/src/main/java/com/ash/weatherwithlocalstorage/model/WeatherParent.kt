package com.ash.weatherwithlocalstorage.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WeatherParent(
    @Expose
    @SerializedName("timezone")
    val timezone: String? = null,

    @Expose
    @SerializedName("current")
    val current: WeatherCurrent? = null,

    @Expose
    @SerializedName("hourly")
    val hourly: List<WeatherHourly>? = null,

    @Expose
    @SerializedName("cod")
    val cod: String? = null,

    @Expose
    @SerializedName("message")
    val message: String? = null

){

}