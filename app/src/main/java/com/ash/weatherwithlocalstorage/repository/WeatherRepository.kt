package com.ash.weatherwithlocalstorage.repository

import com.ash.weatherwithlocalstorage.api.WeatherApi
import com.ash.weatherwithlocalstorage.db.dao.WeatherDao
import com.ash.weatherwithlocalstorage.db.entity.CurrentDataEntity
import com.ash.weatherwithlocalstorage.db.entity.WeekDataEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeatherRepository(private val api: WeatherApi, private val weatherDao: WeatherDao) {
    lateinit var weekDataEntity: WeekDataEntity
    lateinit var cuurentDataEntity: CurrentDataEntity

    //Remote
    suspend fun getWeatherData() = api.getWeather()

    val currDataDb = weatherDao.findAllCurrent()
    suspend fun saveCurrentDataInDb() {
        withContext(Dispatchers.IO) {
            val response = api.getWeather()
            if (response.isSuccessful) {
                val currData = response.body()

                if (currData != null) {
                    cuurentDataEntity = CurrentDataEntity(
                        0,
                        currData.timezone,
                        currData.current?.weather?.get(0)?.description,
                        currData.current?.temp,
                        currData.current?.pressure,
                        currData.current?.humidity,
                        currData.current?.wind_speed
                    )
                    weatherDao.addCurrent(cuurentDataEntity)
                }

            }
        }
    }

    //RoomDatabase WeekData
    val list: MutableList<WeekDataEntity> = ArrayList()
    val weekDataDb = weatherDao.findAll();

    suspend fun saveWeekDataInDb() {
        withContext(Dispatchers.IO) {
            val response = api.getWeather()
            if (response.isSuccessful) {
                val listHourly = response.body()?.hourly
                if (listHourly != null) {
                    for (i in 0..listHourly.size - 1) {
                        weekDataEntity = WeekDataEntity(
                            i,
                            listHourly.get(i).dt,
                            listHourly.get(i).temp,
                            listHourly.get(i).weather?.get(0)?.icon
                        )
                        list.add(weekDataEntity)
                    }
                    weatherDao.add(list)
                }
            }
        }
    }
}