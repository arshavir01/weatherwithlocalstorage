package com.ash.weatherwithlocalstorage.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ash.weatherwithlocalstorage.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
