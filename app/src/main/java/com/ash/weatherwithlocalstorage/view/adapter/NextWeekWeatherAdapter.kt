package com.ash.weatherwithlocalstorage.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ash.weatherwithlocalstorage.R
import com.ash.weatherwithlocalstorage.db.entity.WeekDataEntity
import com.ash.weatherwithlocalstorage.model.WeatherHourly
import com.ash.weatherwithlocalstorage.utils.Methods
import kotlinx.android.synthetic.main.list_row_nextweek_weather.view.*

class NextWeekWeatherAdapter(val context: Context) :
    RecyclerView.Adapter<NextWeekWeatherAdapter.NextWeekViewHolder>() {
    var listWeatherNextWeek: List<WeatherHourly> = ArrayList()
    var listWeatherRoom: List<WeekDataEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextWeekViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_row_nextweek_weather, parent, false)
        return NextWeekViewHolder(view)
    }

    override fun onBindViewHolder(holder: NextWeekViewHolder, position: Int) {
        if (Methods.isInternetAvailable(context)) {
            val dateWholeMillisecond = listWeatherNextWeek.get(position).dt
            getNormalDataTime(dateWholeMillisecond, holder)
            holder.itemView.dayTemp.text = listWeatherNextWeek.get(position).temp?.toInt().toString()
            val imageUrl = "icons/"+ listWeatherNextWeek.get(position).weather?.get(0)?.icon +".png";
            Methods.setImageFromAssets(context, holder.itemView.conditionIcon_day, imageUrl);

        }else if (listWeatherRoom.size > 0 && !Methods.isInternetAvailable(context)) {
            val dateWholeMillisecond = listWeatherRoom.get(position).dt
            getNormalDataTime(dateWholeMillisecond, holder)
            holder.itemView.dayTemp.text = listWeatherRoom.get(position).temp?.toInt().toString()
            val imageUrl = "icons/"+ listWeatherRoom.get(position).icon +".png";
            Methods.setImageFromAssets(context, holder.itemView.conditionIcon_day, imageUrl);
        }
    }

    private fun getNormalDataTime(dateWholeMillisecond: Long?, holder: NextWeekViewHolder) {
        if (dateWholeMillisecond != null) {
            val dateNormal = Methods.convertLongToTime(dateWholeMillisecond * 1000)
            val dayPart = dateNormal.substring(5, 11)
            val hourPart = dateNormal.substring(11, 13)
            holder.itemView.day.text = dayPart
            holder.itemView.weekDayHour.text = hourPart
        }
    }

    override fun getItemCount(): Int {
        if (Methods.isInternetAvailable(context) && listWeatherNextWeek.size > 0)
            return listWeatherNextWeek.size
        else (listWeatherRoom.size > 0)
            return listWeatherRoom.size

    }

    inner class NextWeekViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
        }
    }

    fun submitNextWeekData(list: List<WeatherHourly>, listRoom: List<WeekDataEntity>) {
        listWeatherNextWeek = list
        listWeatherRoom = listRoom
    }
}