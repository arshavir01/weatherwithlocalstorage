package com.ash.weatherwithlocalstorage.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ash.weatherwithlocalstorage.R
import com.ash.weatherwithlocalstorage.db.entity.CurrentDataEntity
import com.ash.weatherwithlocalstorage.db.entity.WeekDataEntity
import com.ash.weatherwithlocalstorage.model.WeatherHourly
import com.ash.weatherwithlocalstorage.model.WeatherParent
import com.ash.weatherwithlocalstorage.utils.Constants
import com.ash.weatherwithlocalstorage.utils.Methods
import com.ash.weatherwithlocalstorage.view.adapter.NextWeekWeatherAdapter
import com.ash.weatherwithlocalstorage.viewmodel.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.sub_lay_weather_data.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment(){
    lateinit var receiver: BroadcastReceiver
    private val weatherViewModel by viewModel<WeatherViewModel>()
    private lateinit var nextWeekWeatherAdapter: NextWeekWeatherAdapter

    private var listWeatherNextWeek: List<WeatherHourly> = ArrayList()
    private var listWeatherRoom: List<WeekDataEntity> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchRoomCurrentData()
        setNextWeekRecyclerView()
        broadcastEvent()
    }

    private fun setNextWeekRecyclerView() {
        nextWeekRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL ,false)
            nextWeekWeatherAdapter = NextWeekWeatherAdapter(requireContext())
            nextWeekRecyclerView.adapter = nextWeekWeatherAdapter
        }
    }

    private fun makeRequest(){
        if(Methods.isInternetAvailable(requireContext())){
            weatherViewModel.data.observe(requireActivity(), Observer {
                updateUIFromRemote(it)
                updateRecyclerView(it)
            })
        }

        weatherViewModel.loadingState.observe(requireActivity(), Observer {
            Methods.remoteRequestStatuse(it, mainProgressBar)
        })
    }

    private fun fetchRoomCurrentData() {
       weatherViewModel.currDataDbVM.observe(requireActivity(), Observer {
            if (!Methods.isInternetAvailable(requireContext()) && it != null)
                updateUIFromDB(it)
            else if(!Methods.isInternetAvailable(requireContext()))
                Methods.openWiFiSettings(requireContext())
        })
    }

    private fun fetchRoomWeekData(){
        weatherViewModel.weekDataDbVM.observe(requireActivity(), Observer {
            if(it.size > 0 && !Methods.isInternetAvailable(requireContext())){
                listWeatherRoom = it
                setAdapter()
            }
        })
    }

    private fun updateUIFromRemote(it: WeatherParent){
        descriptionTv.text = it.current?.weather?.get(0)?.description
        cityName.text = it.timezone?.substring(5)
        tempTv.text = it.current?.temp?.toInt().toString()
        humidityValueTv.text = it.current?.humidity.toString()
        windSpeedValueTv.text = it.current?.wind_speed.toString()
        preasureTempValueTv.text = it.current?.pressure.toString()
    }

    private fun updateUIFromDB(it: CurrentDataEntity){
        cityName.text = it.timezone?.substring(5)
        descriptionTv.text = it.description
        tempTv.text = it.temp?.toInt().toString()
        humidityValueTv.text = it.humidity.toString()
        windSpeedValueTv.text = it.wind_speed.toString()
        preasureTempValueTv.text = it.pressure.toString()
    }

    private fun updateRecyclerView(it: WeatherParent){
        listWeatherNextWeek = it.hourly!!
        setAdapter()
    }

    private fun setAdapter() {
        nextWeekWeatherAdapter.submitNextWeekData(listWeatherNextWeek, listWeatherRoom)
        nextWeekWeatherAdapter.notifyDataSetChanged()
    }

    private fun broadcastEvent(){
        val filter = IntentFilter()
        filter.addAction(Constants.CONNECTIVITY_ACTION)
        receiver = object : BroadcastReceiver(){
            override fun onReceive(p0: Context?, p1: Intent?) {
                if(Methods.isInternetAvailable(requireContext())){
                    offlineIndicator.visibility = View.GONE
                    makeRequest()
                }else{
                    offlineIndicator.visibility = View.VISIBLE
                    fetchRoomWeekData()
                }
            }
        }
      requireActivity().registerReceiver(receiver, filter)
    }

    override fun onDestroy() {
        requireActivity().unregisterReceiver(receiver)
        super.onDestroy()
    }




}
