package com.ash.weatherwithlocalstorage.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ash.weatherwithlocalstorage.model.WeatherParent
import com.ash.weatherwithlocalstorage.repository.WeatherRepository
import com.ash.weatherwithlocalstorage.utils.LoadingState
import kotlinx.coroutines.*

class WeatherViewModel(private val repo: WeatherRepository) : ViewModel() {
    val weekDataDbVM = repo.weekDataDb
    val currDataDbVM = repo.currDataDb

    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState

    private val _data = MutableLiveData<WeatherParent>()
    val data: LiveData<WeatherParent>
        get() = _data

    init {
        fetchRemoteData()
        fetchLocalData()
    }

    private fun fetchRemoteData() {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                val response = repo.getWeatherData()
                if(response.isSuccessful){
                    _data.value = repo.getWeatherData().body()
                    _loadingState.value = LoadingState.LOADED
                }else
                    _loadingState.value = LoadingState.error("Not Successful")
            } catch (e: Exception) {
                _loadingState.value = LoadingState.error(e.message)
            }
        }
    }

    private fun fetchLocalData(){
        viewModelScope.launch {
            try {
                repo.saveWeekDataInDb()
                repo.saveCurrentDataInDb()
            } catch (e: Exception) {
                println(e.message)
            }
        }
    }
}