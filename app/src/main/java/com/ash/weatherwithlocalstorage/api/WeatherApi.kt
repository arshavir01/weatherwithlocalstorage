package com.ash.weatherwithlocalstorage.api

import com.ash.weatherwithlocalstorage.model.WeatherParent
import com.ash.weatherwithlocalstorage.utils.Constants
import retrofit2.Response
import retrofit2.http.GET

interface WeatherApi {
    @GET(Constants.END_POINT)
    suspend fun getWeather(): Response<WeatherParent>

}