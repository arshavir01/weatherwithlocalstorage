package com.ash.weatherwithlocalstorage.di

import android.app.Application
import androidx.room.Room
import com.ash.weatherwithlocalstorage.api.WeatherApi
import com.ash.weatherwithlocalstorage.db.AppDatabase
import com.ash.weatherwithlocalstorage.db.dao.WeatherDao
import com.ash.weatherwithlocalstorage.repository.WeatherRepository
import com.ash.weatherwithlocalstorage.utils.Constants
import com.ash.weatherwithlocalstorage.viewmodel.WeatherViewModel
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    single { WeatherViewModel(get())}
}

val apiModule = module {
    fun provideUseApi(retrofit: Retrofit): WeatherApi {
        return retrofit.create(WeatherApi::class.java)
    }
    single { provideUseApi(get()) }
}

val retrofitModule = module {
    fun provideGson(): Gson {
        return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create()
    }

    fun provideHttpClient(): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
        return okHttpClientBuilder.build()
    }

    fun provideRetrofit(factory: Gson, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(factory))
            .client(client)
            .build()

    }

    single { provideGson() }
    single { provideHttpClient() }
    single { provideRetrofit(get(), get()) }
}

val databaseModule = module {
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "eds.database")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    fun provideDao(database: AppDatabase): WeatherDao {
        return database.weatherDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideDao(get()) }

}

val repositoryModule = module {
    fun provideWeatherRepository(api: WeatherApi, dao: WeatherDao): WeatherRepository{
        return WeatherRepository(api, dao)
    }

    single { provideWeatherRepository(get(), get()) }
}





