package com.ash.weatherwithlocalstorage.utils

class Constants {
    companion object{
        private const val API_KEY = "07c2cec609851b780bbb495834301cf7"

        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val END_POINT = "onecall?lat=40.1872&lon=44.5152&appid=$API_KEY&units=metric&%20exclude=daily"
        const val CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE"
    }
}


