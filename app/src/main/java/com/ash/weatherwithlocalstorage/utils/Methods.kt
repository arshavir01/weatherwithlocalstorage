package com.ash.weatherwithlocalstorage.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.provider.Settings
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import com.ash.weatherwithlocalstorage.R
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

class Methods {
    companion object{
        @SuppressLint("SimpleDateFormat")
        fun convertLongToTime(time: Long): String {
            val date = Date(time)
            val format = SimpleDateFormat("yyyy.MM.dd HH:mm")
            return format.format(date)
        }

        fun openWiFiSettings(context: Context){
                Toast.makeText(context, context.resources.getString(R.string.no_internet), Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                context.startActivity(intent)
        }

        fun remoteRequestStatuse(it: LoadingState, mainProgressBar: ProgressBar){
            when (it.status) {
                LoadingState.Status.FAILED -> mainProgressBar.visibility = View.GONE
                LoadingState.Status.RUNNING -> mainProgressBar.visibility = View.VISIBLE
                LoadingState.Status.SUCCESS -> mainProgressBar.visibility = View.GONE
            }
        }

        fun setImageFromAssets(context: Context, imageView: ImageView, imageUrl: String?) {
            val assetManager = context.assets
            try {
                val ims: InputStream = assetManager.open(imageUrl!!)
                val d = Drawable.createFromStream(ims, null)
                imageView.setImageDrawable(d)
            } catch (ignored: IOException) {
            }
        }

        fun isInternetAvailable(context : Context) : Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork?.isConnectedOrConnecting == true
        }

    }
}