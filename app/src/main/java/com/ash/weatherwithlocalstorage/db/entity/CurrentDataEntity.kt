package com.ash.weatherwithlocalstorage.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "current_weather")
data class CurrentDataEntity(
    @PrimaryKey
    val id: Int?,
    val timezone: String?,
    val description: String?,
    val temp: Double?,
    val pressure: Int?,
    val humidity: Int?,
    val wind_speed: Double?
){}