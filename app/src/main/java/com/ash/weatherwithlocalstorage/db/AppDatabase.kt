package com.ash.weatherwithlocalstorage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ash.weatherwithlocalstorage.db.dao.WeatherDao
import com.ash.weatherwithlocalstorage.db.entity.CurrentDataEntity
import com.ash.weatherwithlocalstorage.db.entity.WeekDataEntity

@Database(entities = [WeekDataEntity::class, CurrentDataEntity::class], version = 5, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){
    abstract val weatherDao: WeatherDao
}