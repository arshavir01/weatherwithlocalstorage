package com.ash.weatherwithlocalstorage.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ash.weatherwithlocalstorage.db.entity.CurrentDataEntity
import com.ash.weatherwithlocalstorage.db.entity.WeekDataEntity

@Dao
interface WeatherDao {
    //Current data
    @Query("SELECT * FROM current_weather")
    fun findAllCurrent(): LiveData<CurrentDataEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCurrent(current: CurrentDataEntity)

    //Week data
    @Query("SELECT * FROM weather")
    fun findAll(): LiveData<List<WeekDataEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(week: List<WeekDataEntity>)


}