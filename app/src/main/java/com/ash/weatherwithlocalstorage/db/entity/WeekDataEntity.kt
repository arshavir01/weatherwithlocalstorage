package com.ash.weatherwithlocalstorage.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "weather")
data class WeekDataEntity(
    @PrimaryKey val id: Int?,
    val dt: Long?,
    val temp: Double?,
    val icon: String?
) {
}